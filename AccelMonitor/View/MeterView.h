//
//  MeterView.h
//  AccelMonitor_iOS
//  Created by Matthew Ferguson on 2/21/16.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> // Layer level graphic support
#import <math.h>  // layout math and is used.
#import "MeterNeedle.h"


@interface MeterView : UIView
{
    
	float minNumber;   // minimum number on arc lines for meter layer.
	float maxNumber;   // max number on arc lines for meter layer.
	double startAngle; // start point for trig math.
	double arcLength;  // length of line.
    float needleValue; // actual float number for needle value represents G-Force.
    
    IBOutlet UILabel * labelMeterName; // meter label name.
	CALayer *needleLayer; // needle layer.
	MeterNeedle *needle;  // needle object.

}

@property (nonatomic, assign)   float         minNumber;
@property (nonatomic, assign)   float         maxNumber;
@property (nonatomic, assign)   double        startAngle;
@property (nonatomic, assign)   double        arcLength;
@property (nonatomic, assign)   float         needleValue;

@property (nonatomic, strong)   IBOutlet  UILabel * labelMeterName;
@property (nonatomic, strong)             MeterNeedle * needle;


- (id)   initWithFrame:(CGRect)frame;
- (void) drawRect:(CGRect)rect;
- (void) awakeFromNib;
- (void) initialize;
- (void) setValue:(float)val;


@end
