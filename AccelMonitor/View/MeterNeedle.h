//
//  MeterNeedle.h
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/21/16.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> // Layer level graphic support
#import <math.h>  // layout math and is used.


@interface MeterNeedle : UIView {
    UIColor *tintColor;
    float length;
    float width;
}

@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, assign) float length;
@property (nonatomic, assign) float width;


- (void)drawLayer:(CALayer*)layer inContext:(CGContextRef)ctx;



@end
