//
//  MeterView.m
//  AccelMonitor_iOS
//  Created by Matthew Ferguson on 2/21/16.
//


#import "MeterView.h"

@implementation MeterView

@synthesize minNumber, maxNumber;
@synthesize startAngle, arcLength;
@synthesize needleValue, labelMeterName, needle;





- (id)initWithFrame:(CGRect)frame {

    if (self = [super initWithFrame:frame]){  }
    return self;
}





// Allow Interface Builder (IB), Main.storyboard, to grant customization of this
// view located on the MeterView.storyboard View Controller.
- (void)awakeFromNib {
	[self initialize];
}




- (void)drawRect:(CGRect)rect  {  }




- (void)initialize
{
    
    
    self.layer.borderColor = [UIColor grayColor].CGColor;
    self.layer.borderWidth = 0.80f;
    self.layer.cornerRadius = 20.0f;
    
    
    // MeterNeedle initialization
    // The needle layer is dependant on these variables.
	self.minNumber  = 0.0;
	self.maxNumber  = 1.0; //initialize so that the needle first shows close to 0 AND 1 on meter.
	self.startAngle = M_PI;
	self.arcLength  = 2.0f * M_PI / 2.0f;
    
    // create a needle init.
	needle = [[MeterNeedle alloc] init];
    
	self.needle.width = 2.0;
	self.needle.length = 0.8f;// 80% of the radius within the rectangle layer.
	
	// Set the needleLayer and the needle object as the delegate.
    needleLayer = [CALayer layer];
	needleLayer.bounds = self.bounds;
	needleLayer.position = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.height / 2.0);
	needleLayer.needsDisplayOnBoundsChange = YES;
	needleLayer.delegate = self.needle;
	
	[self.layer addSublayer:needleLayer];
	
	[needleLayer setNeedsDisplay];

}



// Set the value for this meter view then transform a rotation for the needle subview.
- (void)setValue:(float)val
{
    
    // val hit the max
    if (val > self.maxNumber)
    {
        val = self.maxNumber;
    }
    
    // does the val less than
    if (val < self.minNumber)
    {
        val = self.minNumber;
    }

    // Find the angle of the needle within the rectangle layer.
    // Trig math calculation
    CGFloat angle = self.startAngle + arcLength * val / (self.maxNumber - self.minNumber) - arcLength * (self.minNumber / (self.maxNumber - self.minNumber));
    
    
    [UIView animateWithDuration:0.75f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^{
                         needleLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1);
                     } completion:^(BOOL finished){
                         //NSLog(@"Animation is finished");
                     }];

    
}



@end