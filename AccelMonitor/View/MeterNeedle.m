//
//  MeterNeedle.m
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/21/16.
//

#import "MeterNeedle.h"


@implementation MeterNeedle

@synthesize length, width;
@synthesize tintColor;


- (void)drawLayer:(CALayer*)layer inContext:(CGContextRef)ctx
{
    
    CGContextSaveGState(ctx);
    
    CATransform3D transform = layer.transform;
    
    layer.transform = CATransform3DIdentity;
    
    CGContextSetFillColorWithColor(ctx, tintColor.CGColor);
    CGContextSetStrokeColorWithColor(ctx, tintColor.CGColor);
    CGContextSetLineWidth(ctx, self.width);
    
    CGFloat centerX = layer.frame.size.width / 2.0;
    CGFloat centerY = layer.frame.size.height / 2.0;
    
    CGFloat ellipseRadius = self.width * 4.0;
    
    CGContextFillEllipseInRect(ctx, CGRectMake(centerX - ellipseRadius, centerY - ellipseRadius, ellipseRadius * 2.0, ellipseRadius * 2.0));
    
    CGFloat endX = (1 + self.length) * centerX;
    
    CGContextBeginPath(ctx);
    CGContextMoveToPoint(ctx, centerX, centerY);
    CGContextAddLineToPoint(ctx, endX, centerY);
    CGContextStrokePath(ctx);
    
    layer.transform = transform;
    
    layer.backgroundColor = [UIColor clearColor].CGColor;
    
    CGContextRestoreGState(ctx);
    
}


@end
