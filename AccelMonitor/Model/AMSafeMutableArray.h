//
//  AMSafeMutableArray.h
//  AccelMonitor
//
//  Created by Matthew Ferguson on 8/1/15.
//  Copyright (c) 2015 Matthew Ferguson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMSafeMutableArray : NSMutableArray
{
    NSRecursiveLock *lock;
}

@property (nonatomic, retain) NSRecursiveLock *lock;



@end
