//
//  AMSafeMutableArray.m
//  AccelMonitor
//
//  Created by Matthew Ferguson on 8/1/15.
//  Copyright (c) 2015 Matthew Ferguson. All rights reserved.
//

#import "AMSafeMutableArray.h"

@implementation AMSafeMutableArray

@synthesize lock;

- (void)addObject:(id)obj
{
    [self.lock lock];
    [super addObject: obj];
    [self.lock unlock];
}

- (id) retrieveFIFOObject
{
    [self.lock lock];
    [super addObject: obj];
    [self.lock unlock];
}



- (id)queueHead
{
    
    [self.lock lock];
    
    id localVal = nil;
    if ([super count] == 0)
    {
        localVal = nil;
    }
    else
    {
        localVal
    }
    
    return [super objectAtIndex:0];
}




- (__autoreleasing id) dequeue
{

    if ([super count] == 0)
    {
        return nil;
    }
    
    id head = [super objectAtIndex:0];
    if (head != nil)
    {
        // [[head retain] autorelease]; ARC - the __autoreleasing on the return value should so the same thing
        [super removeObjectAtIndex:0];
    }
    
    return head;

}




- (id)pop
{
    return [super dequeue];
}



- (void)enqueue:(id)object
{
    [super addObject:object];
}






@end
