//
//  AppDelegate.m
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end


@implementation AppDelegate

@synthesize phClient, motionManager;



- (BOOL)application:(UIApplication *)application
                didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    phClient = [self sharedPubHubClient];
    return YES;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}






- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // If the app goes into the background stop the motion sampling.
    // The View Controllers start the updates, handle responder chain, and ...
    //[motionManager stopDeviceMotionUpdates];
    
    // PubHub Unsubscribe to a channel.
    [self.phClient addListener:self];
    [phClient unsubscribeFromChannels:@[@"MattFerguson_PubNub_Test"] withPresence:NO];

}



- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    phClient = [self sharedPubHubClient];
    
    //[motionManager startDeviceMotionUpdates];
    //  the pubnub.com will always have been initialized.
    //  Subscribe to this channel.
    
    [self.phClient addListener:self];
    
    [phClient subscribeToChannels:@[@"MattFerguson_PubNub_Test"] withPresence:YES];

}








- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}





- (void)applicationWillTerminate:(UIApplication *)application {
    // PubHub Unsubscribe to a channel.
    [self.phClient addListener:self];
    [phClient unsubscribeFromChannels:@[@"MattFerguson_PubNub_Test"] withPresence:NO];
    [motionManager stopDeviceMotionUpdates];
     motionManager = nil;
    
    
    //[self saveContext];
}



#pragma - mark ----------------------
#pragma - mark Delegate Method PubNub


- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message
{
    
    
    // Handle new message stored in message.data.message
    if (message.data.actualChannel)
    {
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else
    {
        
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    
    #if (Debug_PubNub)
    NSLog(@"Received message: ''%@'' on channel ''%@'' at ''%@'' ", message.data.message,
          message.data.subscribedChannel, message.data.timetoken);
    #endif
    
    
}




- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status
{
    
    
    #if (Debug_PubNub)
    NSLog(@"didReceiveStatus: %@ ", status.stringifiedCategory );
    #endif
    
    
    
    if (status.category == PNUnexpectedDisconnectCategory)
    {
        // This event happens when radio / connectivity is lost
        
    }
    
    else if (status.category == PNConnectedCategory)
    {
        
        // Connect event. You can do stuff like publish, and know you'll get it.
        // Or just use the connected event to confirm you are subscribed for
        // UI / internal notifications, etc
        
        [phClient publish:@"Pub AccelMonitor Obj-C iOS SDK" toChannel:@"MattFerguson_PubNub_Test"
           withCompletion:^(PNPublishStatus *status)
         {
             
             // Check whether request successfully completed or not.
             if (!status.isError)
             {
                 
                 // Message successfully published to specified channel.
             }
             // Request processing failed.
             else
             {
                 
                 // Handle message publish error. Check 'category' property to find out possible issue
                 // because of which request did fail.
                 //
                 // Request can be resent using: [status retry];
             }
             
         }];
        
    }
    else if (status.category == PNReconnectedCategory)
    {
        // Happens as part of our regular operation. This event happens when
        // radio / connectivity is lost, then regained.
        
    }
    else if (status.category == PNDecryptionErrorCategory)
    {
        // Handle messsage decryption error. Probably client configured to
        // encrypt messages and on live data feed it received plain text.
    }

    
}



#pragma - mark ----------------------------
#pragma - mark AppDelegate Reference PubNub


- (PubNub *)sharedPubHubClient
{
    
    static dispatch_once_t phOnceToken;
    
    dispatch_once(&phOnceToken, ^{
        
        /*
        PNConfiguration *configuration =
        [PNConfiguration
            configurationWithPublishKey:@"pub-c-1a6f4405-9713-48ef-8c03-18151a674c01"
            subscribeKey:@"sub-c-8e08327e-37f4-11e5-99b4-02ee2ddab7fe"];
        */
        
        
        PNConfiguration *configuration =
        [PNConfiguration
         configurationWithPublishKey:@"<YOUR_PUBLISH_KEY_HERE>"
         subscribeKey:@"<YOUR_SUBSCRIBE_KEY_HERE>"];

        
        phClient = [PubNub clientWithConfiguration:configuration];
        [phClient addListener:self];
        [phClient subscribeToChannels:@[@"MattFerguson_PubNub_Test"] withPresence:YES];

        
        if(Debug_PNLog){
            [PNLog enabled:YES];
        }
        else{
            [PNLog enabled:NO];
        }
        
        
    });
    
    return phClient;
}



#pragma - mark AppDelegate Reference CMMotionManager

- (CMMotionManager *)sharedMotionManager{

    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        motionManager = [[CMMotionManager alloc] init];
    });
    
    return motionManager;
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mobilesandbox.AccelMonitor" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



- (NSManagedObjectModel *)managedObjectModel {
    
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AccelMonitor" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AccelMonitor.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}





- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}



#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
