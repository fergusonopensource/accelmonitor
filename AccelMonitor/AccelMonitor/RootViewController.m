//
//  ViewController.m
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void) viewDidAppear:(BOOL)animated {
    NSTimeInterval seconds = 1;
    [self performSelector:@selector(backgroundMethod) withObject:nil afterDelay:(seconds)];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void) backgroundMethod {
    [self performSegueWithIdentifier:@"Root_Meter_Segue" sender:self];
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Root_Meter_Segue"]) {
    }
}



@end
