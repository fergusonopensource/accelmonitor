//
//  ViewController.h
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController

-(void) backgroundMethod;

@end

