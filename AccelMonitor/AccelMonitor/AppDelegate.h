//
//  AppDelegate.h
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import <CoreMotion/CoreMotion.h>
#import <PubNub/PubNub.h>
#import "AMCommonHeader.h"





@interface AppDelegate : UIResponder <UIApplicationDelegate, PNObjectEventListener> {
    
    CMMotionManager * motionManager; //retained and running at app delegate level
    PubNub          * phClient;      // retained and running at app delegate level

}

@property (strong, nonatomic) UIWindow *window;


@property (strong, nonatomic)           PubNub * phClient;
@property (strong, nonatomic, readonly) PubNub * sharedPubHubClient;

@property (strong, nonatomic, readonly) CMMotionManager *sharedMotionManager;
@property (strong, nonatomic)           CMMotionManager *motionManager;


// future core data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;



@end

