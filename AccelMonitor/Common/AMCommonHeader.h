//
//  AMCommonHeader.h
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 8/1/15.


#ifndef AccelMonitor_AMCommonHeader_h
#define AccelMonitor_AMCommonHeader_h


#define Debug_Motion  0
#define Debug_PubNub  0
#define Debug_PNLog   0
#define Debug_View    0
#define Debug_VC      0

#endif
