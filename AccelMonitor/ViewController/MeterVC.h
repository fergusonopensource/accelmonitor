//
//  MeterVC.h
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> // Layer level graphic support
#import "MeterView.h"
#import <CoreMotion/CoreMotion.h>  // Added to support the core motion
#import "AppDelegate.h" // Added for the delegate
#import "PubNub/PubNub.h"  // PubHub library symbols.
#import "AMCommonHeader.h" // common pre-compile and global header location
#import <math.h>  // layout math and is used.


@class MeterView;


@interface MeterVC : UIViewController{
    IBOutlet MeterView * accumMeterVC;
    IBOutlet MeterView * accelMeterVC;
    
    float  accumZVal;
}


@property (nonatomic,strong) IBOutlet MeterView * accumMeterVC;
@property (nonatomic,strong) IBOutlet MeterView * accelMeterVC;
@property (nonatomic, assign)          float      accumZVal;


- (void) viewDidLoad;
- (void) viewDidAppear:(BOOL)animated;
- (void) didReceiveMemoryWarning;
- (void) applicationEnteredForeground:(NSNotification *)notification;
- (void) startMotionAndWebServicesUpdates;
- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator;


@end

