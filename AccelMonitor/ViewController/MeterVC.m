//
//  MeterVC.m
//  AccelMonitor_iOS
//
//  Created by Matthew Ferguson on 2/7/16.
//

#import "MeterVC.h"


@interface MeterVC ()

@end


@implementation MeterVC

@synthesize accelMeterVC, accumMeterVC;
@synthesize accumZVal;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
     accumZVal = 0.0f; // accum from start to shutdown of app.
    
    [self customizeZAccelMeterView];
    [self customizeZAccumMeterView];
    
}




-(void) viewWillDisappear:(BOOL)animated {
    [self.view.layer removeAllAnimations];
}

-(void) viewDidAppear:(BOOL)animated
{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    
    if (deviceOrientation == UIDeviceOrientationPortrait ||
        deviceOrientation == UIDeviceOrientationFaceUp ||
        deviceOrientation == UIDeviceOrientationFaceDown ){
        
        #if (Debug_View)
            NSLog(@"Portrait - accum NO , accel YES ");
        #endif
        
        [self.accumMeterVC setHidden:NO];//accum
        [self.accelMeterVC setHidden:YES];//accel
    
    }
    else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown){
    
        #if (Debug_View)
        NSLog(@"Portrait UpsideDown - accum NO, accel YES");
        #endif
        
        [self.accumMeterVC setHidden:NO];
        [self.accelMeterVC setHidden:YES];
    }
    else if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
    
        #if (Debug_View)
        NSLog(@"Landscape Left - accum NO, accel YES");
        #endif
        
        [self.accumMeterVC setHidden:YES];
        [self.accelMeterVC setHidden:NO];
    }
    else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
    
        #if (Debug_View)
        NSLog(@"Landscape Right UpsideDown - accum NO, accel YES");
        #endif
        
        [self.accumMeterVC setHidden:YES];
        [self.accelMeterVC setHidden:NO];
        
    }
    
    [self startMotionAndWebServicesUpdates];
    
}



- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}



// Customize the Accumulation accelerator Z value meter view
- (void) customizeZAccumMeterView {
    
    self.accumMeterVC.startAngle = -3.0 * M_PI / 4.0;
    self.accumMeterVC.arcLength = M_PI / 2.0;
    self.accumMeterVC.value = 0.0;
    self.accumMeterVC.minNumber = 0.0;
    self.accumMeterVC.maxNumber = 200.0;
    self.accumMeterVC.labelMeterName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:32.0];
    self.accumMeterVC.needle.tintColor = [UIColor blackColor];
    [self.accumMeterVC setHidden:YES];
    
}





// Customize the accelerator Z value meter view
- (void) customizeZAccelMeterView {
    
    self.accelMeterVC.startAngle = -3.0 * M_PI / 4.0;
    self.accelMeterVC.arcLength = M_PI / 2.0;
    self.accelMeterVC.value = 0.0;
    self.accelMeterVC.minNumber = 0.0;
    self.accelMeterVC.maxNumber = 1.0;
    self.accelMeterVC.labelMeterName.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:32.0];
    self.accelMeterVC.labelMeterName.textColor = [UIColor redColor];
    self.accelMeterVC.needle.tintColor = [UIColor redColor];
    [self.accelMeterVC setHidden:YES];
    
}



#pragma mark ----------------------
#pragma mark Motion and Comm Utility


- (void)applicationEnteredForeground:(NSNotification *)notification {
    [self startMotionAndWebServicesUpdates];
}





- (void) startMotionAndWebServicesUpdates
{

    
    NSTimeInterval updateInterval = 0.1; // .025 is 25 milli-seconds.
    CMMotionManager *mManager = [(AppDelegate *)[[UIApplication sharedApplication] delegate] sharedMotionManager];
    
    // Start CMMotionManager motion update (at a periodic rate), threaded off,
    //  on a block within the main thread/queue
    if ([mManager isDeviceMotionAvailable] == YES)
    {
        
        [mManager setDeviceMotionUpdateInterval:updateInterval];
        
        // mainQueue adds that block to the operation queue of the mainthread but does not guarantee
        // when it will be executed. There could be other items in that queue still waiting to execute.
        // "deviceMotion" , not a global variable that can be overwritten, should be allocated and
        // in memory of the block placed on the mainQueue.
        
        [mManager startDeviceMotionUpdatesToQueue:
         [NSOperationQueue mainQueue] withHandler:^(CMDeviceMotion *deviceMotion, NSError *error)
         {
             
             //load local instances within this block.
             accumZVal = accumZVal + fabs(deviceMotion.userAcceleration.z);
             [self.accumMeterVC setValue:accumZVal];
             
             [self.accelMeterVC setValue:fabs(deviceMotion.userAcceleration.z)];
             
             #if (Debug_Motion)
             NSLog(@"startDeviceMotionUpdatesToQueue : outside GCD block, deviceMotion.userAcceleration.z %lf", deviceMotion.userAcceleration.z);
             #endif
             
             // Seperate the PubHub publish to prevent contention between Motion Manager
             // and the PubHub updates.
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 
                 NSDictionary *JSONDic=[[NSDictionary alloc]
                                        initWithObjectsAndKeys:
                                        [NSString stringWithFormat:@"%lf",deviceMotion.userAcceleration.z],
                                        @"ZVal",
                                        [NSString stringWithFormat:@"%lf",deviceMotion.userAcceleration.z],
                                        @"AccumZVal", nil];
                 
                 #if (Debug_Motion)
                 NSLog(@"startDeviceMotionUpdatesToQueue : inside GCD block, JSONDic %@ ", JSONDic);
                 #endif
                 
                 PubNub *phClient = [(AppDelegate *)[[UIApplication sharedApplication] delegate] sharedPubHubClient];
                 
                 [phClient publish:JSONDic toChannel:@"MattFerguson_PubNub_Test"
                    storeInHistory:YES
                    withCompletion:^(PNPublishStatus *status)
                  {
                      // Check whether request successfully completed or not.
                      if (!status.isError)
                      {
                          
                          // Message successfully published to specified channel.
                          #if (Debug_Motion)
                          NSLog(@"accelThreadedPublish : Message successfully published to specified channel");
                          #endif
                          
                      }
                      // Request processing failed.
                      else
                      {
                          // Handle message publish error. Check 'category' property to find out possible issue
                          // because of which request did fail.
                          //
                          // Request can be resent using: [status retry];
                          #if (Debug_Motion)
                          NSLog(@"accelThreadedPublish : message publish error.");
                          #endif
                      }
                      
                  }];//end of completion block for PubHub client publish.
             });// end of completion block, CMMotionManager
         }];// end of CMMotionManager mainQueue
        
    }
    
}



#pragma mark View Controller Orientation
- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context){
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        
        if (deviceOrientation == UIDeviceOrientationPortrait ){
            
            [self.accumMeterVC setHidden:NO];
            [self.accelMeterVC setHidden:YES];
        }
        else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown){
            [self.accumMeterVC setHidden:NO];
            [self.accelMeterVC setHidden:YES];
        }
        else if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
            [self.accumMeterVC setHidden:YES];
            [self.accelMeterVC setHidden:NO];
        }
        else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
            [self.accumMeterVC setHidden:YES];
            [self.accelMeterVC setHidden:NO];
        }
    }
    completion:^(id<UIViewControllerTransitionCoordinatorContext> context){

    }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end