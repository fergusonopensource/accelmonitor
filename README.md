# README #

### What is this repository for? ###

* Quick summary

NOTE: A PUBNUB.COM ACCOUNT IS REQUIRED AND THE SUB/PUB PRIVATE KEYS ASSOCIATED WITH IT WILL NEED TO BE CHANGED WITHIN THE SOURCE CODE. SEE BELOW FOR DETAILS.

An Example iOS project/workspace that shows PubNub.com API use, transfer of the accelerometer run-time data of the iOS device, and displays this data on a SUM and run-time custom meter view(s).  View transition is triggered by the the rotation of the device.  Landscape shows run-time accelerometer Z coordinate meter and portrait will show the accumulation (SUM) of accelerometer Z meter since starting the app. The motion processing continues as long as the app is running, but stops updating the views past 200 Pub-Sub passed JSON packets.  All values are packaged as a JSON packet and Published on the PUB/SUB PubNub.com network at a rate of 100ms.  Complete publish-subscription transfer takes less than 102ms per frame. 

* Version

Version 1.0(1)

* Summary of set up

Open this project from the Xcode workspace file using Xcode 7.x SDK 9.0. Do not open this project from the Xcode project file due to the use and dependencies of the CocoaPods libraries integration. 

* Configuration

Setup a PubNub.com free account. Make sure you understand what is free.  At the time of the latest submission free meant under 100 devices. This app needs the pub and sub private account keys. Input the string into the AppDelegate.m file within the Xcode workspace.